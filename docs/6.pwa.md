# Progressive Web App

The blog include the PWA configuration:

- The manifest file [src\themes\mediumish\static\manifest.json](../src/themes/mediumish/static/manifest.json)
- The worker service file [src\themes\mediumish\static\sw.js](../src/themes/mediumish/static/sw.js)
- The integration of the worker service [src\themes\mediumish\layouts\partials\_shared\js.html](../src/themes/mediumish/layouts/partials/_shared/js.html)
- The icon in different sizes [src\themes\mediumish\static\appicons](../src/themes\mediumish/static/appicons)

## Known issue

The cache is not yet configure properly and you may experience issues when testing your new article locally.

A dirty quick fix is to tick the box `Bypass for network` in the chrome developer tool/application/services Workers

![pwa-bypass-for-network](images/1.pwa-bypass-for-network.png)
