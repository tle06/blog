---
title: '{{name}}'
description: ''
date: 2020-09-14T08:00:00.000Z
lastmod: '2021-09-26T11:57:31.541Z'
publishdate: 2020-09-14T08:00:00.000Z
draft: true
tags: null
categories: null
comments: false
type: post
language: English
slug:
---

Table example
{{< table "table table-striped table-bordered table-hover" >}}
| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  left-aligned | $1600 |
| col 2 is |    centered   |   $12 |
| col 3 is | right-aligned |    $1 |
{{</ table >}}frontMatter.createContent


