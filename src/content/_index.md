---
title: "Thomas"
type: "page"
comments: false
layout: "about"
picture: "/images/author.webp"
subtitle: "What about sharing back"
alertbar: false
---

Internet gave and still give me most of my IT technical knowledge. I spent hours reading tutorial, blog post and thread on internet.
I think it's now my time to share my experience, my knowledge and my skill that I acquired for the past 15 years.
Through this blog, I will share all the random stuff that I use, do, live, hoping will help the beginner or anyone who dare read me.

Mucho mucho ❤️
