---
title: How I increase by 70% internet connection with 4G
date: 2020-09-14T08:00:00.000Z
publishdate: 2020-09-14T08:00:00.000Z
lastmod: '2021-09-26T14:25:42.742Z'
tags:
    - network
type: post
comments: false
categories:
    - network
keywords:
    - 4G
    - internet
    - network
    - speed
description: How I increase by 70% my internet connection speed with a 4G connection.
slug: increase-70-internet-connection-4g
---

My parents are leaving in France, in the middle of nowhere. They have 2Mb/s download and 1Mb/s upload ADSL line.

Impossible to watch Netflix, play online, make a Skype call or download your brother PlayStation game update (70Gb). I'm not speaking about the service interruption cause by the worker or the tractor pulling up the line. Moreover, the house is in a hole and there is no GSM/3G/4G signal whatsoever.

Unfortunately, I still like my family and I still enjoy spending a few days in the countryside, but I also need to work from home and with the current situation, it's just impossible. Also, optical fiber connectivity is not planned before 2025... I definitely needed to find a way to improve their internet connection.

![init_speed_test](images/03_init_speedtest.webp#center)

## 1. Action Plan

The only way to improve is to use a 4G connection, but as I said, there is no signal in the house. I know that a 4G+ signal is available 2 km away on the main road, and I also received a weak signal 250 m away of the house. My guess is that the house is surrounding of 4G signal but since it's on a hole, the signal pass above the house. I need to find a way to catch up the signal and bring it to the house.

Action plan:

- Finding the closest 4G BTS in the area
- Finding the elevation of the house and the road
- Finding a 4G antenna with enough gain

## 2. Bit of studies

### 2.1 The BTS

I start looking on internet and I found a [website](https://www.ariase.com/mobile/carte-antennes?iframe=1&partner=ariase&lat=46.22545263071106&lng=1.8896485000000496&zoom=5.318176029308926&interactive=1) that shows the BTS register in France with the associate provider.
There are 2 BTS around the house, one at 3 km and one at 5 km crow flies distance. Only the provider "Orange" is available with 4G+ signal in the area.

### 2.2 Elevation

Since there is signal on the road, I tried to estimate the size of the hole where the house is located. I used this [website](https://www.daftlogic.com/sandbox-google-maps-find-altitude.htm) to search the elevation base on the address.

{{< table "table table-striped table-bordered table-hover" >}}
| Sites                 | Elevation (m) |
|-----------------------|---------------|
| House                 | 189           |
| End of the house road | 198           |
| Main road             | 208           |
{{</ table >}}

Approximately there is 19 m gap between the road and the house, and 9 m from the house and the end of its access road (where I get the first 4G signal). The top of the chimney is at 7 m from the ground, reducing the total gap at 12 m from the main road and 2 m from the access road.

So to start getting a 4G signal, I need to place the antenna between 2 m and 12 m above the chimney. The first options seem more realistic than the second one.

### 2.3 The pole

My dad crafts a lot of stuff and a brief look into his stuff, we manage to find a round pole, 4 m long. We agreed to use 1 m to fix it onto the chimney giving 3 m elevation above it. According to the previous section, it should be enough to get a signal, especially if the antenna has enough gain.

### 2.4 The Antenna

I needed to find an antenna that can received 4G with a good DB gain but also convert the signal to Ethernet.

> Why? You need the shortest distance between the antenna itself and the electronique card that will process the signal. If you use a long cable, you will have lost and all the work performed by your antenna will be gone. Also 10m of shield coaxial cable cost a fortune. The best is to used RJ45 cable that can go up to 100m, especially the CAT6 shield that can handle 1Gb/s over 100m for few euros.

{{< table "table table-striped table-bordered table-hover" >}}
| cable   | distance (m) | cost (€) |
|---------|--------------|----------|
| RJ45    | 100          | 40       |
| Coaxial | 100          | 171      |
{{</ table >}}

After 1 or 2 hours on Google, I hand up on this [website](https://techvorace.com/), discussing 4G router antenna. You have several models of antenna.

- Use an [external antenna](https://www.amazon.fr/LowcostMobile-RAD58-MIMO-700MAX-11dbi-Huawei-Netgear/dp/B071GK4JNV/ref=as_li_ss_tl?srs=14311696031&ie=UTF8&qid=1559990850&sr=8-7&th=1&linkCode=sl1&tag=techvoracecom-21&linkId=2d7019a02029e409b4be31345a30761f&language=fr_FR) and a [router](https://teltonika-networks.com/product/rut950/), pack into a waterproof box
- Use all in one antenna like the [Microtik SXT LTE](https://www.amazon.fr/MikroTik-RBSXTR-R11e-LTE-SXT-LTE/dp/B07DGNHX27/ref=as_li_ss_tl?_encoding=UTF8&me=&linkCode=sl1&tag=techvoracecom-21&linkId=271796983a987e740e653bfc60dcc835&language=fr_FR)

I went for the second option, easy to fix on the pole, cost-efficient and already pack into a waterproof case. Moreover, the antenna can be power through POE, handle 2 sim cards and has 9 dbi gain. On the bad side, the bandwidth can't exceed 100Mb/. Not a big deal in my case, since the closest BTS can only deliver 150Mb/s max.

### 2.5 The SIM card

The most important was to find an offer than work without having using the provider router. If you target classic mobile phone offer it should work most of the time since there is no hardware binding.

I found one provider that propose unlimited 4G data. Unfortunate for me, I don't have enough signal to receive a correct bandwidth.

- [Bouygues Telecom](https://www.bouyguestelecom.fr/offres-internet/4g-box)

The rest have between 100Gb and 200Gb per month.

- [NRJ](https://www.nrjmobile.fr/fr/) up to 130Gb/month
- [Red by SFR](https://www.red-by-sfr.fr/forfaits-mobiles/#redintid=B_HP_voir-offre-mobile) up to 200Gb/month
- [Prixtel](https://www.prixtel.com/) up to 200Gb/month
- [Sosh](https://shop.sosh.fr/mobile/forfaits-mobiles?MCO=OFR&doNotTryToAuthenticateAgain=true) up to 80Gb/month and something they have offers for 100Gb/month. You can also add 50Gb with extra money.

Those offers are not working since they required the provider router:

- [Orange](https://m.boutique.orange.fr/eligibilite/internet-4g-a-la-maison) up to 200Gb/month

In my case, I needed Orange or a provider using Orange network since it is the only provider available.
I choose [Prixtel](https://www.prixtel.com/) they offer up to 200Gb/month for 22 €. The quantity is enough for my family and the price remain low. You can easily buy everything online, the sim card is ship within a week, and you can cancel your subscription at anytime for 0 €. They also have a mobile app where you can follow your current consumption.

> Check also the procedure of the provider, you may need to activate the sim card first. it's the case for `Sosh`. When I tried this provider, I add to enable the sim card by calling a number. I will also recommend putting the sim card into a mobile find, drive to a zone where you have coverage and test it out. Because once it's in the antenna, it's a nightmare to change or removed.

### 2.6 APN

To configure your sim card in the antenna you will need the APN configuration.
I used this [website](https://www.apnsettings.org/france/orange-fr/) to find the information.
  
## 3. Time for magic

### 3.1 Shopping list

{{< table "table table-striped table-bordered table-hover" >}}
| Item               | Quantity | Total cost (€) |
|--------------------|----------|----------------|
| [20 m Ethernet cable](https://www.amazon.fr/20m-Blanc-Ethernet-R%C3%A9seau-Compatible/dp/B00VC01HNQ/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=ethernet+cable+20m+cat6&qid=1600964289&s=computers&sr=1-3) | 1        | 17             |
| [4G router antenna](https://www.amazon.fr/MikroTik-RBSXTR-R11e-LTE-SXT-LTE/dp/B07DGNHX27/ref=as_li_ss_tl?_encoding=UTF8&me=&linkCode=sl1&tag=techvoracecom-21&linkId=271796983a987e740e653bfc60dcc835&language=fr_FR)  | 1        | 130            |
| 4 m pole            | 1        | 10             |
| [Tie Down Straps](https://www.amazon.fr/professionnel-Sangles-pi%C3%A8ces-Cliquet-certifi%C3%A9/dp/B07767TCBP/ref=sr_1_2_sspa?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=IG68LTPCHRCU&dchild=1&keywords=sangle+a+cliquet&qid=1600964436&sprefix=sangle%2Caps%2C196&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzMU1GTTk4WDRHVzVYJmVuY3J5cHRlZElkPUEwNDYxODMxMUVHSEFDNzRXWEdFSSZlbmNyeXB0ZWRBZElkPUEwNzEwMDQ2Vks1RzhNWjBZQUdaJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==)    | 2        | 16             |
| [20 m Cable duct](https://www.amazon.fr/Janoplast-JAN100595-Gaine-lubrifi%C3%A9e-Diam%C3%A8tre/dp/B00FIZM4SS/ref=sr_1_2?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=gaine+electrique+20mm&qid=1600964373&sr=8-2)      | 1        | 8             |
| **Total**            |          | **181**          |
{{</ table >}}

> The ethernet cable will be passing through cable duct to protect it from the outdoor element.
> The Tie Down Straps will be used to fix the pole to the chimney.

### 3.2 Installation

#### 3.2.1 Antenna configuration

You will have to configure the ASN of the SIM card.
> I strongly advise to do it before putting the antenna on the pole and on your chimney. You will also have to prepare the network configuration, DHCP, relay, subnet,...

1. Connect to the local IP of the antenna, by default `192.168.88.1`.
2. Login as admin with the username `admin` the password is empty by default.
3. Click on `Terminal` on the top right of the interface
4. Type the following command

```bash
/interface lte apn add name=orange apn=orange.fr authentication=pap password=orange user=orange
/interface lte set [find] apn-profiles=orange
```

5. If you need a pin code for the sim card

```bash
/interface lte set [find] pin=0000
```

6. Check if the config is applied

```bash
/interface lte print
```

The expected result should be

```bash
Flags: X - disabled, R - running 
 0  R name="lte1" mtu=1500 mac-address=AA:AA:AA:AA:AA:AA
```

> By default the bridge mode was enable on my antenna. If you need passthrough, dual sim or more config, you can refer to the [documentation of routerOS](https://wiki.mikrotik.com/wiki/Manual:Interface/LTE).

#### 3.2.2 The physical part

The pole with the antenna on the top fix to the chimney:

![pole fixed with antenna](images/01_pole_antenna-min.webp#center)

The cable duct protecting the RJ45 cable from the roof to the inside of the house:

![protecting duct](images/02_protection-min.webp#center)

In my specific case the RJ45 cable arrived directly into the house rack and plugged to a [LEGRAND patch panel](https://www.rexel.fr/frx/Cat%C3%A9gorie/R%C3%A9seau-informatique/Produit-cuivre/Panneau-de-brassage-cuivre/Panneau-brassage-droit-19pouces-1U-LCS%C2%B3-cat%C3%A9gorie6-avec-24-connecteurs-RJ45-FTP/LEG033761/p/71111280?isPaginationRequest=true) that is connected to a [Ubiquity POE+ switch](https://eu.store.ui.com/collections/unifi-network-routing-switching/products/usw-24-poe).

> My dad has specific need about his network. I will probably make a blog post about it one day.

You can also connect it to your ISP box, switch or router as long you do not forget to add the POE injector (it's provided with the antenna). If you are using your ISP box, don't forget to change the DHCP setting on the antenna or the ISP box and either disable one or set one as a relay. Then the gateway should be the antenna local IP.

### 3.3 Result

We place the pole above the chimney, after few seconds the Microtik router start getting the signal and I could do the first speed test, 30Mb/s download, 20Mb/s up. The increase is already insane, but we start turning the antenna in different direction to see if we could get closer to the 100Mb/s. After few tries and 20 speed test, we found a spot where the signal was up to 70Mb/s download and 40Mb/s upload.

![final_speed_test](images/04_final_speed_test.webp#center)

> Before fixing the pole to the chimney, we run a quick validation test of the concept. After perparing the pole, we plug a 10m RJ45 cable (powered with the POE adaptor from the antenna) to my laptop.

## 4. Conclusion

With around 200 € I managed to multiply by x35 the download speed and by x40 the upload speed.

![bravo](images/05.bravo.jpeg#center)

----

Sources

- [Antenna quick guide](https://i.mt.lv/cdn/product_files/SXTmB_130559.pdf)
- [Antenna user manual](https://i.mt.lv/cdn/product_files/SXTug_130502.pdf)
- [Microtik router configuration documentation](https://wiki.mikrotik.com/wiki/Manual:TOC)
