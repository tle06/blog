---
title: How to reach zero email in your inbox
date: '2021-10-01T08:00:00.000Z'
publishdate: '2021-10-01T08:00:00.000Z'
lastmod: '2021-10-01T14:27:21.871Z'
description: "How to reach Zero email inbox easily by following few rules and guideline. Concentrate on what matter, and archived everything else."
draft: false
tags:
  - productivity
type: post
comments: false
categories:
  - productivity
language: English
slug: how-to-reach-zero-email-inbox
---

First thing first, I don't like emails, I don't like to send/received them and seeing my inbox full drive me, somehow, crazy.
But, after few years of struggle and practice, I managed to achieve the zero inbox status almost every day. I concentrate on the 80% of my inbox with the less time as possible, The rest can be archived 🗄️.

## 1. My inbox, my rules

Basically, I have few rules that I always followed.

1. Emails are asynchronous medium of communication.
2. I do not read my email where I'm in CC
3. The Do Not Read folder (DNR)
4. I click systematically on the unsubscribe button
5. I block senders whenever I can
6. I archived everything and clean my inbox

### 1.1 Emails are asynchronous

Indeed, email is not a phone call or a face-to-face conversation. If I want something done right away, I don't send an email. I will give a phone call or Slack/Teams my team, or move my butt around the office for a quick chat. With these rules in mind, I have zero pressure to answers right away my emails ⛱️. However, in the opposite, I do not expect people to answer me straight away.

### 1.2 CC is for information only

Everyone knows or should know that CC is for sharing information only. You also don't know if the recipient will actually give a damn about it or not. So, I do not expect to receive a direct task/request/order when my email address is in CC and because of that, I do not read them...

> Due to the nature of my work, I already have a team reporting 90% of the information I received in CC. I'm confortable to lose the remaining 10%.

### 1.3 The DNR folder

One of my best founding so far, is the DNR folder. Every email that I believe I should not receive or not read goes directly into this balck hole without being read. Few examples:

- I always expect a meeting room to accept my booking request because I chose an available time slot. So, the successful confirmation goes to the DNR folder
- Same when I invite someone to a meeting, the successful confirmation goes to DNR because I chose the right time slot at the beginning
- Admin confirmation email that I can't unsubscribe goes there
- Monitoring email where I can't remove myself due to a mailing list condition
- Email in a language I can't read, example spanish, italian
- A ticket update or comment from jira
- All the Out Of Office (OOO) message. Some systems are smart and will send you this message only once, some are not. I usually don't care and send them directly to the DNR folder.

### 1.4 Unsubscribe everything

I received a sh*t ton of newsletters that I never subscribe to (this peace me off). So, every time I received those kind of email, I take 10s of my time to click on the unsubscribe button (at the bottom) and uncheck all the box if needed. If the link is not available, right click report as spam 💥.

### 1.5 Block sender

I also received a lot of email to present product or commercial offer, presentations and a ton of fancy stuff. I usually discard the first email, if they insist, I simply block the sender, efficiency 💯.

### 1.6 Archived everything

Every time I finished an email, either reply, read, create a backlog,.. I mark it as read and archived it. For that, I use a lot the following shortcuts:

**G-mail:**

- mark as read `SHIFT+I`
- archived `E`

**Outlook:**

- mark as read `CRTL+Q`
- archived `E`

> On my smarphone I configure those short cut, `sliding left` will mark as read when `sliding right` will archive the message.

Archiving your email will remove them from inbox and place them into the `archived` (in outlook) or `all message` (g-suite) folder. If you do it every time you go through an email, you will see that your inbox end up with zero email quickly.

## 2. Back from holidays

You had two great week for yourself, without phone call, colleagues, Slack messages and emails. It was perfect right. But it's not indefinitely. Usually, its means that the last Sunday evening is focus on your mailbox, trying to go through those emails before starting your busy expected busy monday.

![wrong](images/wrong.webp#center)

I have been doing some statistics over the past few years, below it's an example of my last holidays period.

{{< table "table table-striped table-bordered table-hover" >}}
|Time of|Received|Sent|ratio|
|-------|--------|----|-----|
|2 weeks|211     |48  |23%  |
|1 week |112     |11  |10%  |
{{</ table >}}

I noticed that reading my (super filtered) emails after long time off (> week), it's not useful at all. In reality, My team give me 90% of the information during the first team meeting or stand-up after my holidays period. The answer I sent its most of the time an approval or an acknowledgement. The rest is non-important, and if it’s the case, do not worry the sender will send it back with something like `any news on this topic` or `update please`.

That why, when I'm back from holidays, I do select all, mark as read, archived. Boom, I just save 2h of reading on my last Sunday of holidays 😎.

## 3. Spend less time as possible on email

Also, something I like to do, is to dedicated time to read my email. I have usually three 10min slot per day.

- 10 min on the morning before the coffee with the team, 9:00 (am)
- 10 min before or after lunch, 14:00 or 15:00
- 10 min before leaving the office, 18:15

At the end of the day, I do not spend more than 30 min on average looking into my mailbox.

## 4. Conclusion

Discard as much as possible the email without any value.
Chill out, it's not an synchronous communication medium.
Actually, stop ending email 💡.

![meme](images/email_meme.webp#center)
