# TLNK blog

The blog is currently using [hugo](https://gohugo.io/) with the theme [Mediumish](https://github.com/lgaida/mediumish-gohugo-theme).

## Documentation

1. [Local development](docs/1.local-development.md)
2. [Docker build](docs/2.docker-build.md)
3. [Article structure](docs/3.article-structure.md)
4. [Release process](docs/4.release-process.md)
   1. [Articles to markdown tool](docs/4.1.articles-to-md-tool.md)
5. [Frontmatter vs code extension](docs/5.frontmatter.md)
6. [Progressive Web App](docs/6.pwa.md)
