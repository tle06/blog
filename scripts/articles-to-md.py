#!/usr/local/bin/python

import os
import yaml
import argparse

def get_front_matter(file:str,encoding:str="utf-8"):
    with open(file,encoding=encoding) as f:
        front_matter:dict = next(yaml.load_all(f, Loader=yaml.FullLoader))
    return front_matter

def generate_md_hyperlink(title:str,url:str):
    md: str = f"- [{title}]({url})"
    return md

def gen_url(root_url:str,slug:str):
    return f"{root_url}{slug}"

def get_files(path:str,file_extension:str=".md"):
    filelist = []

    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(file_extension):
                filelist.append(os.path.join(root,file))
    return filelist

def main():

    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('-s', '--source',
                        dest='source',
                        help='The folder where the blog article are located',
                        required=True,
                        type=str
                        )
    parser.add_argument('-o', '--output',
                        default='blog.md',
                        dest='output',
                        help='output path for the list of article in markdown',
                        type=str
                        )
    parser.add_argument('-r', '--open-mode',
                        default='w',
                        dest='open_mode',
                        help='python open mode value, default w',
                        type=str
                        )
    parser.add_argument('-e', '--file-extension',
                        default='.md',
                        dest='file_extension_filter',
                        help='The file extension to look for, default .md',
                        type=str
                        )
    parser.add_argument('-u', '--root_url',
                        default="https://www.tlnk.fr/blog/",
                        dest='root_url',
                        help='The root url of the blog post to generate the markdown hyperlink',
                        type=str
                        )                                         
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Verbose Output'
                        )
    args = parser.parse_args()
    
    # Args to var

    source:str = args.source
    output:str = args.output
    root_url:str = args.root_url
    open_mode:str = args.open_mode
    file_extension:str = args.file_extension_filter if args.file_extension_filter else ""
    verboseprint = print if args.verbose else lambda *a, **k: None

    verboseprint("Start script")
    verboseprint(f"Source: {source}")
    verboseprint(f"Output: {output}")
    verboseprint(f"Root URL: {root_url}")
    verboseprint(f"File extension: {file_extension}")

    filelist: list = get_files(path=source,file_extension=file_extension)
    markdown: list = []
    verboseprint(f"Number fo files found: {len(filelist)}")

    # generate markdown

    if not filelist:
        exit(0)

    for name in filelist:
        verboseprint(f"File found: {name}")
        front_matter: dict = get_front_matter(file=name)
        title:str = front_matter['title']
        verboseprint(f"Title found: {title}")
        slug:str = front_matter['slug']
        verboseprint(f"Slug found: {slug}")
        url: str = gen_url(root_url=root_url,slug=slug)
        verboseprint(f"URL generated: {url}")
        markdown_url:str = generate_md_hyperlink(title=title,url=url)
        verboseprint(f"Markdown generated: {markdown_url}")
        markdown.append(markdown_url)

    # write markdown
    verboseprint(f"Write markdown at: {output}")
    verboseprint(f"Number of row to write: {len(markdown)}")

    file = open(output, open_mode)
    verboseprint(f"File opened")
    
    for items in markdown:
        file.writelines(items+'\n')
        verboseprint(f"{items} written into: {output}")
    file.close()
    verboseprint(f"File closed")

    verboseprint("Done")

if __name__ == "__main__":
    main()